extends VBoxContainer

signal choice_chosen

var player

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node("/root/Node2D/VBoxContainer/InkPlayer")


func _on_InkPlayer_InkContinued(text, tags):
	delete_choices_nodes()


func delete_choices_nodes():
	for n in get_children():
		n.queue_free()


func _on_InkPlayer_InkChoices(choices):
	delete_choices_nodes()
	
	var i = 0
	for c in choices:
		var node = Button.new()
		node.text = c
		node.align = Button.ALIGN_CENTER
		node.connect("pressed", player, "ChooseChoiceIndexAndContinue", [i])
		
		add_child(node)
		
		i += 1
