extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var scroll_container: ScrollContainer
var v_box_container: VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	scroll_container = get_node("ScrollContainer")
	v_box_container = get_node("ScrollContainer/VBoxContainer")


func _on_InkPlayer_InkContinued(text, _tags):
	# Build a new node and add it to the scroll view
	var label = Label.new()
	label.autowrap = true
	label.text = text
	v_box_container.add_child(label)
	
	# Scroll to the bottom of the scroll view
	scroll_container.scroll_vertical = scroll_container.get_v_scrollbar().max_value
