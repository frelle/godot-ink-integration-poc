extends Label

var player
var choices_container: VBoxContainer

# Called when the node enters the scene tree for the first time.
func _ready():
	player = get_node("/root/Node2D/InkPlayer")
	choices_container = get_node("/root/Node2D/VBoxContainer")

func _on_InkPlayer_InkChoices(choices):
	delete_choices_nodes()
	
	var i = 0
	for c in choices:
		var node = Button.new()
		node.text = c
		node.align = Button.ALIGN_CENTER
		node.connect("pressed", player, "ChooseChoiceIndexAndContinue", [i])
		
		choices_container.add_child(node)
		
		i += 1


func _on_InkPlayer_InkContinued(text, _tags):
	delete_choices_nodes()
	self.text = text


func delete_choices_nodes():
	for n in choices_container.get_children():
		n.queue_free()


func _on_InkPlayer_InkError(message, isWarning):
	self.text = "Error: %s" % message


func _on_Button_pressed():
	player.Continue()
